2014-08-27 16:53:09 -0700 Adam Crews 

	* Add variable validations and spec tests to support (HEAD, variable_valiation)

2014-07-28 07:39:13 -0700 Adam Crews 

	* Updated .fixtures to use forge modules.

2014-07-26 10:39:38 -0700 Adam Crews 

	* Added some spec tests (origin/refactor)

2014-07-25 23:29:49 -0700 Adam Crews 

	* Updated readme

2014-07-24 11:17:06 -0700 Adam Crews 

	* Refactor module to make it more extendable

2014-07-23 08:57:01 +1000 Timothy Mukaibo 

	* Source default variables if they exist

2014-07-23 07:14:57 +1000 Timothy Mukaibo 

	* Improve status()

2014-07-22 08:07:16 +1000 Timothy Mukaibo 

	* Replace Modulefile with metadat.json (tag: v1.1)

2014-07-22 07:58:09 +1000 Timothy Mukaibo 

	* Fix Status()

2014-07-22 07:18:18 +1000 Timothy Mukaibo 

	* Improve console output

2014-07-22 07:14:05 +1000 Timothy Mukaibo 

	* Forgot to pass args

2014-07-21 13:39:58 +1000 Timothy Mukaibo 

	* Update init script for Debian

2014-07-21 08:52:36 +1000 Timothy Mukaibo 

	* Add a crappy init script for Debian

2014-07-18 09:15:46 +1000 Timothy Mukaibo 

	* Correct typo

2014-07-18 08:37:06 +1000 Timothy Mukaibo 

	* Add init script for Centos / RedHat

2014-07-14 08:02:55 +1000 Timothy Mukaibo 

	* Add contrib package to postgres server

2014-07-12 17:46:31 +1000 Timothy Mukaibo 

	* Add modulefile (tag: v1.0)

2014-07-12 17:39:24 +1000 Timothy Mukaibo 

	* Update toc

2014-07-12 17:38:25 +1000 Timothy Mukaibo 

	* Add a name to the readme

2014-07-12 17:37:16 +1000 Timothy Mukaibo 

	* Hopefully make example clearer

2014-07-12 17:34:48 +1000 Timothy Mukaibo 

	* Add parameter documentation

2014-07-12 17:20:18 +1000 Timothy Mukaibo 

	* Add a readme

2014-07-12 10:07:52 +1000 Timothy Mukaibo 

	* Move variables to init

2014-07-09 19:58:09 +1000 Timothy Mukaibo 

	* Update path

2014-07-09 19:55:00 +1000 Timothy Mukaibo 

	* Trying to get rbenv into /home/bamboo

2014-07-09 18:52:01 +1000 Timothy Mukaibo 

	* Change installpath

2014-07-09 17:23:55 +1000 Timothy Mukaibo 

	* make rbenv global

2014-07-09 07:29:01 +1000 Timothy Mukaibo 

	* Add rbenv for bamboo plugin

2014-07-08 17:52:38 +1000 Timothy Mukaibo 

	* Update bamboo home setting

2014-07-08 17:22:04 +1000 Timothy Mukaibo 

	* Update requirement to be the extracted bamboo

2014-07-08 13:52:32 +1000 Timothy Mukaibo 

	* Extract as bamboo user

2014-07-08 13:44:23 +1000 Timothy Mukaibo 

	* Correct typo

2014-07-08 08:49:32 +1000 Timothy Mukaibo 

	* They are files, silly

2014-07-08 08:46:31 +1000 Timothy Mukaibo 

	* Correct typo

2014-07-08 08:41:19 +1000 Timothy Mukaibo 

	* Download and extract bamboo

2014-07-08 07:51:34 +1000 Timothy Mukaibo 

	* Split module out

2014-07-07 11:42:59 +1000 Timothy Mukaibo 

	* Remove variables

2014-07-07 11:07:07 +1000 Timothy Mukaibo 

	* Correct style

2014-07-07 08:50:12 +1000 Timothy Mukaibo 

	* Switch to v7

2014-07-07 08:37:16 +1000 Timothy Mukaibo 

	* Add postgres configuration

2014-07-07 08:06:56 +1000 Timothy Mukaibo 

	* Initial commit

